﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ConnectionToDB
{
    public partial class User
    {
        public int Id { get; set; }
        public string Adress { get; set; }
        public string Name { get; set; }
    }
}
