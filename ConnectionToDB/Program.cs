﻿using System;
using System.Linq;

namespace ConnectionToDB
{
    internal static class Program
    {
        static void Main(string[] args)
        {
            using (FirstefdatabaseContext db = new FirstefdatabaseContext())
            {
                var users = db.Users.ToList();
                Console.WriteLine("List of users:");
                foreach (User i in users)
                {
                    Console.WriteLine($"{i.Id}.{i.Name} - {i.Adress}");
                }
            }
        }
    }
}
