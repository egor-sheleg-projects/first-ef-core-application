# First EF Core application

## Purpose  
By the end of the task, you will be able to create a class and appropriate Entity Framework classes & methods that can be used to connect and store data in the MS SQL local database. The task consists of two stages. In the first stage, you will create a project that shows how to store data in a database. In the second stage, you will create a project that demonstrates how to read data from a database using the Entity Framework. 

**Estimated time to complete:** 2 hours.

**Task status:** mandatory / manually-checked.  

## Description

### Stage #1. Store Data in a Database

1. Create a new project. Use the `Console App (.NET Core)` VS 2019 project type.
1. Add the NuGet package that contains EntityFramework Core and allows you to work with MS SQL Server database:
    
    ![](Pictures/ef1.png) 

1. Add a class that will describe the data that will be stored in the database.
1. Add 3 properties that will be stored in DB.
    - Every property will be a column in the database table.
    - ID will be used as a primary key by default.

    ![](Pictures/ef2.png) 

1. Add the `DbContext` class to the project.
    
    ![](Pictures/ef3.png) 

1. In the `Main` method. add the client code to open the data context connection and add several records to the database.
   
    ![](Pictures/ef4.png) 

1. Run the application and make sure it works.
1. Add the functionality of updating a user.

    ![](Pictures/ef5.png) 
    
### Stage #2. Connect to the previously created database and read data from it

1. Create another new `Console App (.NET Core)` VS 2019 project type.  
1. Name it `ConnectionToDB`.
1. Add 2 NuGet packages:
    - `Microsoft.EntityFrameworkCore.SqlServer`
    - `Microsoft.EntityFrameworkCore.Tools`
1. You should get:

 ![](Pictures/ef6.png) 

1. Use the Reverse Engineering  feature of the Entity Framework  and create a class that represents records from the existing database created in the first project: `Database=firstefdatabase`.
1. Open the Package  Manager Console:    
    
    `Tools –> NuGet Package Manager –> Package Manager Console`

1. Run the scaffolding command:   
    
    `Scaffold-DbContext "Server=(localdb)\mssqllocaldb;Database= firstefdatabase;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer`    

1. After running this command, two new classes will be added to the project: the `User` and `DbContext` classes.
1. Add the client code to the `Main` method and check that everything works:
    - Open the database context.
    - Read the data from the database and output it to the console.
   
    ![](Pictures/ef7.png) 

    - Close the DB context.

1. Run the application and make sure it works and reads data from the database


