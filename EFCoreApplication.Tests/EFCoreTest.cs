﻿using System;
using System.Collections.Generic;
using System.IO;
using EFCoreApp;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace EFCoreApplication.Tests
{
    /// <summary>
    /// Check all database user funtions.
    /// </summary>
    [TestClass]
    public class EFCoreTest
    {
        /// <summary>
        /// Testing deleting from database.
        /// </summary>
        [TestMethod]
        public void GetTest()
        {
            // arrange 
            var dbcontroller = new DBController();
            string expected = "Done.";

            // act
            string result = dbcontroller.GetUsers();

            // assert            
            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Testing adding to database.
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            // arrange 
            var dbcontroller = new DBController();
            string arg1 = "Name";
            string arg2 = "Adress";
            string expected = "Done.";

            // act
            string result = dbcontroller.AddUser(arg1, arg2);

            // assert            
            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Testing update in database.
        /// </summary>
        [TestMethod]
        public void UpdateTest()
        {
            // arrange 
            var dbcontroller = new DBController();
            string arg1 = "Name";
            string arg2 = "New Adress";
            string expected = "Done.";

            // act
            string result = dbcontroller.UpdateUser(arg1, arg2);

            // assert            
            Assert.AreEqual(expected, result);
        }
    }
}
