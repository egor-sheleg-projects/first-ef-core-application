﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace EFCoreApp
{
    internal static class Program
    {
        static void Main(string[] args)
        {
            DBController dbController = new DBController();
            string button = string.Empty;
            while(button != "q")
            {
                Console.WriteLine("Enter a - to add user in database, g to see all database users, u to update first user in database");
                button = Console.ReadLine();
                switch (button)
                {
                    case "a":
                        {
                            Console.WriteLine("Enter name:");
                            string name = Console.ReadLine();
                            Console.WriteLine("Enter adress:");
                            string adress = Console.ReadLine();
                            string result = dbController.AddUser(name,adress);
                            Console.WriteLine(result);
                            break;
                        }
                    case "g":
                        {
                            string result = dbController.GetUsers();
                            Console.WriteLine(result);
                            break;
                        }
                    case "u":
                        {
                            Console.WriteLine("Enter name:");
                            string name = Console.ReadLine();
                            Console.WriteLine("Enter adress:");
                            string adress = Console.ReadLine();
                            string result = dbController.UpdateUser(name, adress);
                            Console.WriteLine(result);
                            break;
                        }
                }
            }
        }
    }
}
