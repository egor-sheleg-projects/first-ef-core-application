﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EFCoreApp
{
    public class DBController
    {
        public string AddUser(string name, string adress)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                User usr1 = new User { Name = name, Adress = adress };

                db.Users.AddRange(usr1);
                db.Users.Add(usr1);
                db.SaveChanges();

                Console.WriteLine("Sucsessfully stored");

                return "Done.";
            }
        }

        public string GetUsers()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var users = db.Users.ToList();
                Console.WriteLine("List of users:");
                foreach (User i in users)
                {
                    Console.WriteLine($"{i.Id}.{i.Name} - {i.Adress}");
                }

                return "Done.";
            }
        }

        public string UpdateUser(string name, string adress)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                User user = db.Users.FirstOrDefault();

                user.Name = name;
                user.Adress = adress;

                db.Users.Update(user);
                db.SaveChanges();

                return "Done.";
            }
        }
    }
}
